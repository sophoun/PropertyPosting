﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyPosting.Configures.ConfigDB;

namespace PropertyPosting.Models.Repository
{
    public interface IRepository<T>
    {
        void Insert(T t);
        void Update(T t);
        void Delete(T t);
        IEnumerable<T> GetAll();
        T GetById(long id);
    }
}
