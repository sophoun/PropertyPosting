﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using PropertyPosting.Models;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Controllers.MvcControllers
{
    public class UserController : Controller
    {
        private readonly IRepository<User> _userService;

        public UserController(IRepository<User> userService)
        {
            _userService = userService;
        }

        public IEnumerable<User> GetUsers()
        {
            return _userService.GetAll();
        }
    }
}
