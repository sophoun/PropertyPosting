﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains
{
    public class PostImage
    {
        public long Id { set; get; }
        public long PostId { set; get; }
        public string Image { set; get; }
    }
}