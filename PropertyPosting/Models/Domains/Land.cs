﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains
{
    public class Land
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public decimal LandWidth { set; get; }
        [Required]
        public decimal LandLength { set; get; }
        public string Other { set; get; }
    }
}
