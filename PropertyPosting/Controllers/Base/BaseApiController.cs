﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using AutoMapper;
using PropertyPosting.Configures.ConfigDB;
using Unity.Attributes;

namespace PropertyPosting.Controllers.Base
{
    public abstract class BaseApiController<T> : ApiController
    {
        protected readonly IMapper Mapper;

        protected BaseApiController(IMapper mapper)
        {
            this.Mapper = mapper;
        }

        // GET /api/{controller}
        [HttpGet]
        public abstract IHttpActionResult Get();

        // GET /api/{controller}/{id}
        [HttpGet]
        public abstract IHttpActionResult Get(long id);

        // POST /api/{controller}
        [HttpPost]
        public abstract IHttpActionResult Post(T t);

        // PUT /api/{controller}/{id}
        [HttpPut]
        public abstract IHttpActionResult Put(long id, T t);

        // DELETE /api/{controller}/{id}
        [HttpDelete]
        public abstract IHttpActionResult Delete(long id);

    }
}