﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Controllers.MvcControllers.Administrator
{
    public class PostTypeController : BaseController
    {
        private readonly IRepository<PostType> _postTypeService;
        private readonly IMapper _mapper;

        public PostTypeController(IMapper mapper, IRepository<PostType> postTypeService) : base(mapper)
        {
            _mapper = mapper;
            _postTypeService = postTypeService;
        }

        public override ActionResult Index()
        {
            var postTypes = _postTypeService.GetAll();
            return View("~/Views/Administrator/PostType/Index.cshtml", postTypes);
        }

        public ActionResult Add()
        {
            return View("~/Views/Administrator/PostType/Create.cshtml");
        }

        [HttpPost]
        public ActionResult Add(PostType postType)
        {
            ModelState.Remove("Id");
            if (!ModelState.IsValid)
                return View("~/Views/Administrator/PostType/Create.cshtml", postType);

            if (postType.Id > 0)
            {
                var postTypeUpdate = _mapper.Map<PostType>(postType);
                _postTypeService.Update(postTypeUpdate);
            }
            else
            {
                _postTypeService.Insert(postType);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var postType = _postTypeService.GetById(id);
            if (postType != null)
            {
                _postTypeService.Delete(postType);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var postType = _postTypeService.GetById(id);
            return View("~/Views/Administrator/PostType/Create.cshtml", postType);
        }

        public override ActionResult Details(long id)
        {
            throw new NotImplementedException();
        }
    }
}