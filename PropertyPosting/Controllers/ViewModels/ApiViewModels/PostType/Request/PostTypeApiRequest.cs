﻿using System.ComponentModel.DataAnnotations;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Request
{
    public class PostTypeApiRequest
    {
        [Required]
        public string Name { set; get; }
        public string Other { set; get; }
    }
}