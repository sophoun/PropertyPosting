﻿using PropertyPosting.Controllers.ViewModels.ApiViewModels.Entertainment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyPosting.Hubs
{
    public interface IEntertainmentHub
    {
        void PostEntertainment(EntertainmentApiRequestResponse t);
        void UpdateEntertainment(EntertainmentApiRequestResponse t);
        void DeleteEntertainment(long t);
    }
}
