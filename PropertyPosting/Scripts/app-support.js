﻿const app = {};
if (typeof jQuery != 'undefined') {
    $.fn.serializeObject = function () {
        var output = {};
        var input = this.serializeArray();
        $.each(input, function (index, field) {
            output[field.name] = field.value;
        });
        return output;
    };

    $.fn.setField = function (fields = []) {
        var formId = this[0].id;
        $.each(fields, function (index, field) {
            $(`#${formId} [name="${field.name}"]`).val(field.value);
        });
    };
}




//Author James Harrington 2014
function base64(files, callback) {
    var obj = [];
    function readFile(files, index) {
        if (index >= files.length) {
            return;
        }
        var coolFile = {};
        function readerOnload(e) {
            var base64 = btoa(e.target.result);
            coolFile.base64 = base64;
            callback(coolFile);
            readFile(files, index + 1);
        };

        var reader = new FileReader();
        reader.onload = readerOnload;

        var file = files[index];
        coolFile.filetype = file.type;
        coolFile.size = file.size;
        coolFile.filename = file.name;
        reader.readAsBinaryString(file);
    }
    readFile(files, 0);

}