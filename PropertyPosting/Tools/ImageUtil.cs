﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PropertyPosting.Tools
{
    public static class ImageUtil
    {
        private static readonly string _userResourcePath = @"/Resources/UserImages/";
        private static readonly string _staffResourcePath = @"/Resources/StaffImages/";
        private static readonly string _postResourcePath = @"/Resources/PostImages/";
        private static readonly string _entertainmentResourcePath = @"/Resources/EntertainmentVideos/";
        public static string GetImageFileName(string prefix)
        {
            return prefix + "-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff") + ".jpg";
        }
        public static string GetVideoFileName(string prefix)
        {
            return prefix + "-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff") + ".mp4";
        }

        public static string SaveEntertainmentVideo(string base64String, string filename)
        {
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrEmpty(filename))
            {
                return null;
            }
            var videoBytes = Convert.FromBase64String(base64String);
            var storePath = _entertainmentResourcePath + filename;
            var path = HttpContext.Current.Server.MapPath("~" + storePath);
            using (var videoFile = new FileStream(path, FileMode.Create))
            {
                videoFile.Write(videoBytes, 0, videoBytes.Length);
                videoFile.Flush();
            }
            return storePath;
        }
        // Convert base64 string and save local file image and return url path to file
        public static string SaveUserImage(string base64String, string fileName)
        {
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrEmpty(fileName))
                return null;

            var imgBytes = Convert.FromBase64String(base64String);
            var storePath = _userResourcePath + fileName;
            var path = HttpContext.Current.Server.MapPath("~" + storePath);

            using (var imageFile = new FileStream(path , FileMode.Create))
            {
                imageFile.Write(imgBytes, 0, imgBytes.Length);
                imageFile.Flush();
            }

            return storePath;
        }

        // Convert base64 string and save local file image and return url path to file
        public static string SaveStaffImage(string base64String, string fileName)
        {
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrEmpty(fileName))
                return null;

            var imgBytes = Convert.FromBase64String(base64String);
            var storePath = _staffResourcePath + fileName;
            var path = HttpContext.Current.Server.MapPath("~" + storePath);

            using (var imageFile = new FileStream(path, FileMode.Create))
            {
                imageFile.Write(imgBytes, 0, imgBytes.Length);
                imageFile.Flush();
            }

            return storePath;
        }

        // Convert base64 string and save local file image and return url path to file
        public static string SavePostImage(string base64String, string fileName)
        {
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrEmpty(fileName))
                return null;

            var imgBytes = Convert.FromBase64String(base64String);
            var storePath = _postResourcePath + fileName;
            var path = HttpContext.Current.Server.MapPath("~" + storePath);

            using (var imageFile = new FileStream(path, FileMode.Create))
            {
                imageFile.Write(imgBytes, 0, imgBytes.Length);
                imageFile.Flush();
            }

            return storePath;
        }

    }
}