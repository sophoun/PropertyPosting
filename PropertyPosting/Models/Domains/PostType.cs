﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains
{
    public class PostType
    {
        [Key]
        public int Id { set; get; }
        [Required]
        public string Name { set; get; }
        public string Other { set; get; }
        public DateTime TimeStamp { get; set; }
    }
}