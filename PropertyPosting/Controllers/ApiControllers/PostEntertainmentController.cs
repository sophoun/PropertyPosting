﻿ using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Entertainment;
using PropertyPosting.Hubs;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Services;
using PropertyPosting.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace PropertyPosting.Controllers.ApiControllers
{
    [Route("api/post/entertainment")]
    public class PostEntertainmentController : BaseApiController<EntertainmentApiRequestResponse>
    {
        private IMapper _mapper;
        private EntertainmentPostService _entertainmentPostService;
        private readonly IEntertainmentHub _entertainmentHub;
        public PostEntertainmentController(IMapper mapper,
            EntertainmentPostService entertainmentService,
            IEntertainmentHub entertainmentHub): base(mapper)
        {
            _mapper = mapper;
            _entertainmentPostService = entertainmentService;
            _entertainmentHub = entertainmentHub;
        }
        [Route("api/post/entertainment/{id}")]
        public override IHttpActionResult Delete(long id)
        {
            var entertainment = _entertainmentPostService.GetById(id);
            if(entertainment == null)
            {
                return BadRequest("Not Found");
            }
            _entertainmentPostService.Delete(entertainment);
            _entertainmentHub.DeleteEntertainment(entertainment.Id);
            return Ok("Delete Successfully");
        }

        public override IHttpActionResult Get()
        {
            var entertainment = _entertainmentPostService.GetAll();
            var returnEntertainment = _mapper.Map < IEnumerable<EntertainmentApiRequestResponse>>(entertainment);
            return Json(returnEntertainment);
        }
        [Route("api/post/entertainment/{id}")]
        public override IHttpActionResult Get(long id)
        {

            var entertainmentToReturn = GetEntertainment(id);
            if(entertainmentToReturn == null)
            {
                return BadRequest("Not Found");
            }
           
            return Json(entertainmentToReturn);
        }

        private EntertainmentApiRequestResponse GetEntertainment(long id)
        {
            var entertainment = _entertainmentPostService.GetById(id);
            var entertainmentToReturn = _mapper.Map<EntertainmentApiRequestResponse>(entertainment);
            return entertainmentToReturn;
        }



        public override IHttpActionResult Post(EntertainmentApiRequestResponse t)
        {
            if (t == null)
            {
                return BadRequest("Error Model");
            }
            var entertainment = _mapper.Map<EntertainmentPost>(t);
            var videoPath =  ImageUtil.SaveEntertainmentVideo(t.Video, ImageUtil.GetVideoFileName(entertainment.Title));
            entertainment.Video = videoPath;
            _entertainmentPostService.Insert(entertainment);

            var entertainmentToReturn = GetEntertainment(entertainment.Id);
            _entertainmentHub.PostEntertainment(entertainmentToReturn);
            return Json(entertainment);
        }
        [Route("api/post/entertainment/{id}")]
        public override IHttpActionResult Put(long id, EntertainmentApiRequestResponse t)
        {
            var entertainment = _entertainmentPostService.GetById(id);
            if(entertainment == null)
            {
                return BadRequest("Not Found");
            }
            ImageUtil.SaveEntertainmentVideo(t.Video, ImageUtil.GetVideoFileName(t.Title));
            _mapper.Map(t, entertainment);
            _entertainmentPostService.Update(entertainment);
            var entertainmentToReturn = GetEntertainment(entertainment.Id);
            _entertainmentHub.UpdateEntertainment(entertainmentToReturn);
            return Ok("Update Entertainment Post successfully!");
        }
    }
}