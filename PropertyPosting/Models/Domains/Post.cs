﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Models.Domains
{
    public class Post
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public string Title { set; get; }
        [Required]
        public string Description { set; get; }
        [Required]
        public decimal Price { set; get; }
        public long LocationId { get; set; }
        [Required]
        public PostStatus Status { set; get; } // Posting / Sold/ Stop
        [Required]
        public PropertyCondition PropertyCondition { get; set; } // Sale or Rent
        [Required]
        public DateTime? TimeStamp { set; get; }
        [Required]
        public AllowedType Allowed { set; get; }
        [Required]
        public DateTime? AllowedDate { set; get; }
        public long Repost { set; get; }
        public string Other { set; get; }

        public long PostTypeId { set; get; }
        public long UserId { set; get; }
        public long StaffId { get; set; }
        public long HomeId { get; set; }
        public long LandId { get; set; }
    }
}