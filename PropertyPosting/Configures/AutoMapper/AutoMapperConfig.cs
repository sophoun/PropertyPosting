﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.Configuration;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Entertainment;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Home;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Land;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Location;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Staff;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Staff.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Staff.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response;
using PropertyPosting.Models;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Domains.Enums;
using PropertyPosting.Tools;

namespace PropertyPosting.Configures.AutoMapper
{
    public class AutoMapperConfig : Profile
    {

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {

//                cfg.AllowNullCollections = true;
//                cfg.AllowNullDestinationValues = true;
//                cfg.CreateMissingTypeMaps = true;

                // ---> user
                // request
                cfg.CreateMap<UserApiRequest, User>()
                    .ForMember(u => u.Image, opt => opt.MapFrom(u => ImageUtil.SaveUserImage(u.Image, ImageUtil.GetImageFileName(u.UserName))))
                    .ForMember(u => u.Password, opt => opt.MapFrom(u => Util.Encrypt(u.Password)));
                // response
                cfg.CreateMap<User, UserApiResponse>();
                // <---

                // ---> staff
                // request
                cfg.CreateMap<StaffApiViewModel, Staff>()
                    .ForMember(s => s.Image, opt => opt.MapFrom(s => ImageUtil.SaveUserImage(s.Image, ImageUtil.GetImageFileName(s.UserName))))
                    .ForMember(s => s.Password, opt => opt.MapFrom(s => Util.Encrypt(s.Password)));
                // response
                cfg.CreateMap<Staff, StaffApiResponse>();
                // <---

                // ---> postType
                // request
                cfg.CreateMap<PostTypeApiRequest, PostType>();
                // response
                cfg.CreateMap<PostType, PostTypeApiResponse>();
                // <---

                // ---> Location
                // request
                cfg.CreateMap<LocationApiRequestResponse, Location>();
                // response
                cfg.CreateMap<Location, LocationApiRequestResponse>();
                // <---

                // ---> PostHome
                // request
                cfg.CreateMap<PostApiRequest<HomeApiRequestResponse>, Post>();
                // response
                cfg.CreateMap<Post, PostApiResponse<HomeApiRequestResponse>>();
                // <---

                // ---> PostLand
                cfg.CreateMap<PostApiRequest<LandApiRequestResponse>, Post>()
                .ForMember(des => des.PostTypeId, m => m.MapFrom(src => 2))
                .ForMember(des => des.AllowedDate, m => m.MapFrom(src => DateTime.Now))
                .ForMember(des => des.TimeStamp, m=> m.MapFrom(src => DateTime.Now));
                // response
                cfg.CreateMap<Post, PostApiResponse<LandApiRequestResponse>>();
                // <---

                // ---> PostEntertainment
                //Request
                cfg.CreateMap<EntertainmentApiRequestResponse, EntertainmentPost>().
                ForMember(des => des.TimeStamp, m=>m.MapFrom(src => DateTime.Now));
                // Response
                cfg.CreateMap<EntertainmentPost, EntertainmentApiRequestResponse>();
               
                // ---> Home
                // request
                cfg.CreateMap<HomeApiRequestResponse, Home>();
                // response
                cfg.CreateMap<Home, HomeApiRequestResponse>();
                // <---

                // ---> Land
                //request
                cfg.CreateMap<LandApiRequestResponse, Land>();
                // response
                cfg.CreateMap<Land, LandApiRequestResponse>();

            });

            return config.CreateMapper();
        }
    }

}