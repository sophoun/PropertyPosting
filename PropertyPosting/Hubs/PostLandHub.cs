﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Owin;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Land;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;

namespace PropertyPosting.Hubs
{
    public class PostLandHub: Hub, IPostLandHub
    {
        private readonly IHubContext _context;
        public  PostLandHub()
        {
            _context = GlobalHost.ConnectionManager.GetHubContext<PostLandHub>();
        }

        [HubMethodName("deletePostLand")]
        public void DeletePostLand(long id)
        {
            _context.Clients.All.DeletePostLand(id);
        }
        [HubMethodName("postLand")]
        public void PostLand(PostApiResponse<LandApiRequestResponse> t)
        {
            _context.Clients.All.PostLand(t);
        }

      

        [HubMethodName("updateLand")]
        public  void UpdateLand(PostApiResponse<LandApiRequestResponse> t )
        {
            _context.Clients.All.updatePostLand( t ); 
        }
    }
}