﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class PostTypeService : IRepository<PostType>
    {
        private readonly AppDbContext _appDbContext;

        public PostTypeService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(PostType t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.PostTypes.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(PostType t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.PostTypes.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(PostType t)
        {
            _appDbContext.PostTypes.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<PostType> GetAll()
        {
            return _appDbContext.PostTypes.ToList();
        }

        public PostType GetById(long id)
        {
            return _appDbContext.PostTypes.Find(id);
        }
    }
}