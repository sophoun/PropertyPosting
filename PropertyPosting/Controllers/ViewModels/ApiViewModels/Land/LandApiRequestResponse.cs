﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Land
{
    public class LandApiRequestResponse
    {
        
        public long Id { set; get; }
        public decimal LandWidth { set; get; }
        public decimal LandLength { set; get; }
        public string Other { set; get; }
    }
}