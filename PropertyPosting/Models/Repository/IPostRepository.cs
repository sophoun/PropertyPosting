﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyPosting.Models.Domains;

namespace PropertyPosting.Models.Repository
{
    public interface IPostRepository : IRepository<Post>
    {
        IEnumerable<Post> GetAllHomePosts();
        IEnumerable<Post> GetAllLandPosts();
        IEnumerable<Post> GetAllExcavatorPosts();
    }
}
