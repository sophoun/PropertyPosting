﻿using System;
using System.ComponentModel.DataAnnotations;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Location;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request
{
    public class PostApiRequest<T>
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public string Title { set; get; }
        [Required]
        public string Description { set; get; }
        [Required]
        public decimal Price { set; get; }
        [Required]
        public PropertyCondition PropertyCondition { get; set; } // Sale or Rent
        public string Other { set; get; }
        public string[] Images { get; set; }
        public LocationApiRequestResponse Location { get; set; }
        public T PostDetail { get; set; }

        public long PostTypeId { set; get; }
        public long UserId { set; get; }
    }
}