using System.Data.Entity;
using System.Web.Mvc;
using PropertyPosting.Models;
using Unity;
using Unity.Mvc5;
using Unity.Lifetime;
using System.Web.Http;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Repository;
using PropertyPosting.Models.Domains;
using PropertyPosting.Configures.DI;
using Unity.Injection;
using AutoMapper;
using PropertyPosting.Configures.AutoMapper;
using PropertyPosting.Models.Services;
using PropertyPosting.Hubs;

namespace PropertyPosting
{
    public static class UnityConfig
    {
        private static UnityContainer _container;
        public static void RegisterComponents()
        {
			_container = new UnityContainer();

            // register all your components with the _container here
            // it is NOT necessary to register your controllers

            // e.g. _container.RegisterType<ITestService, TestService>();

            // AppDbContext
            _container.RegisterType<AppDbContext, AppDbContext>(new HierarchicalLifetimeManager());

            // ---> Inject Service
            // UserService
            _container.RegisterType<IRepository<User>, UserService>(new HierarchicalLifetimeManager());
            // StaffService
            _container.RegisterType<IRepository<Staff>, StaffService>(new HierarchicalLifetimeManager());
            // PostTypeService
            _container.RegisterType<IRepository<PostType>, PostTypeService>(new HierarchicalLifetimeManager());
            // EntertainmentPostService
            _container.RegisterType<IRepository<EntertainmentPost>, EntertainmentPostService>(new HierarchicalLifetimeManager());
            // LocationService
            _container.RegisterType<IRepository<Location>, LocationService>(new HierarchicalLifetimeManager());
            // LandService
            _container.RegisterType<IRepository<Land>, LandService>(new HierarchicalLifetimeManager());
            // HomeService
            _container.RegisterType<IRepository<Home>, HomeService>(new HierarchicalLifetimeManager());
            // PostImageService
            _container.RegisterType<IPostImageRepository, PostImageService>(new HierarchicalLifetimeManager());
            // PostService
            _container.RegisterType<IPostRepository, PostService>(new HierarchicalLifetimeManager());
            // <--- End Inject Service


            // --> SignalR
            // PostLanf
            _container.RegisterType<IPostLandHub, PostLandHub>(new HierarchicalLifetimeManager());
            _container.RegisterType<IPostHomeHub, PostHomeHub>(new HierarchicalLifetimeManager());
            _container.RegisterType<IEntertainmentHub, EntertainmentHub>(new HierarchicalLifetimeManager());


            // Set dependency resolver
            DependencyResolver.SetResolver(new UnityDependencyResolver(_container));

            // AutoMapper
            _container.RegisterInstance<IMapper>(AutoMapperConfig.GetMapper());


           
        }

        public static void RegisterDependencyResolverForWebApi(HttpConfiguration config)
        {
            config.DependencyResolver = new UnityResolver(_container);
        }
    }
}