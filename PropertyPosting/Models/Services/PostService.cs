﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Domains.Enums;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class PostService : IPostRepository
    {
        private readonly AppDbContext _appDbContext;

        public PostService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(Post t)
        {
            t.Status = PostStatus.Posting;
            t.TimeStamp = DateTime.Now;
            t.Allowed = AllowedType.Pending;
            t.AllowedDate = DateTime.Now;
            _appDbContext.Posts.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(Post t)
        {
            
            _appDbContext.Posts.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(Post t)
        {
            _appDbContext.Posts.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Post> GetAll()
        {
            return _appDbContext.Posts.ToList();
        }

        public Post GetById(long id)
        {
            return _appDbContext.Posts.Find(id);
        }

        public IEnumerable<Post> GetAllHomePosts()
        {
            return _appDbContext.Posts
                .Where(p => p.PostTypeId == 1)
                .ToList();
        }

        public IEnumerable<Post> GetAllLandPosts()
        {
            return _appDbContext.Posts
                .Where(p => p.PostTypeId == 2)
                .ToList();
        }

        public IEnumerable<Post> GetAllExcavatorPosts()
        {
            return _appDbContext.Posts
                .Where(p => p.PostTypeId == 3)
                .ToList();
        }
    }
}