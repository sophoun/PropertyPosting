﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class HomeService : IRepository<Home>
    {
        private readonly AppDbContext _appDbContext;

        public HomeService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(Home t)
        {
            _appDbContext.Homes.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(Home t)
        {
            _appDbContext.Homes.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(Home t)
        {
            _appDbContext.Homes.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Home> GetAll()
        {
            return _appDbContext.Homes.ToList();
        }

        public Home GetById(long id)
        {
            return _appDbContext.Homes.Find(id);
        }
    }
}