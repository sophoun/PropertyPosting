﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class StaffService : IRepository<Staff>
    {
        private readonly AppDbContext _appDbContext;

        public StaffService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(Staff t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.Staffs.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(Staff t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.Staffs.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(Staff t)
        {
            _appDbContext.Staffs.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Staff> GetAll()
        {
            return _appDbContext.Staffs.ToList();
        }

        public Staff GetById(long id)
        {
            return _appDbContext.Staffs.Find(id);
        }
    }
}