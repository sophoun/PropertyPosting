﻿using System.ComponentModel.DataAnnotations;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Location;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response
{
    public class PostApiResponse<T>
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public string Title { set; get; }
        [Required]
        public string Description { set; get; }
        [Required]
        public decimal Price { set; get; }
        [Required]
        public PropertyCondition PropertyCondition { get; set; } // Sale or Rent
        public string Other { set; get; }
        public string[] Images { get; set; }
        public LocationApiRequestResponse Location { get; set; }
        public T PostDetail { get; set; }

        public PostTypeApiResponse PostType { set; get; }
        public UserApiResponse User { set; get; }
    }
}