﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class LocationService : IRepository<Location>
    {
        private readonly AppDbContext _appDbContext;

        public LocationService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(Location t)
        {
            _appDbContext.Locations.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(Location t)
        {
            _appDbContext.Locations.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(Location t)
        {
            _appDbContext.Locations.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Location> GetAll()
        {
            return _appDbContext.Locations.ToList();
        }

        public Location GetById(long id)
        {
            return _appDbContext.Locations.Find(id);
        }
    }
}