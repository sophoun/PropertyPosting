﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class UserService : IRepository<User>
    {
        private readonly AppDbContext _appDbContext;

        public UserService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(User t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.Users.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(User t)
        {
            t.TimeStamp = DateTime.Now;
            _appDbContext.Users.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(User t)
        {
            _appDbContext.Users.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<User> GetAll()
        {
            return _appDbContext.Users.ToList();
        }

        public User GetById(long id)
        {
            return _appDbContext.Users.Find(id);
        }
    }
}