﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class LandService : IRepository<Land>
    {
        private readonly AppDbContext _appDbContext;

        public LandService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(Land t)
        {
            _appDbContext.Lands.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(Land t)
        {
            _appDbContext.Lands.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(Land t)
        {
            _appDbContext.Lands.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<Land> GetAll()
        {
            return _appDbContext.Lands.ToList();
        }

        public Land GetById(long id)
        {
            return _appDbContext.Lands.Find(id);
        }
    }
}