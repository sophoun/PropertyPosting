﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Entertainment
{
    public class EntertainmentApiRequestResponse
    {
        public long Id { set; get; }
        [Required]
        public string Title { set; get; }
        public string Description { set; get; }
        [Required]
        public string Video { set; get; }
        public string Other { set; get; }
    }
}