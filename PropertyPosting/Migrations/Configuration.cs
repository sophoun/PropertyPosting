using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;

namespace PropertyPosting.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            // PostType seed
            context.PostTypes.AddOrUpdate(new PostType(){ Id = 1, Name = "Home", TimeStamp = DateTime.Now });
            context.PostTypes.AddOrUpdate(new PostType(){  Id = 2, Name = "Land", TimeStamp = DateTime.Now });
            context.PostTypes.AddOrUpdate(new PostType(){  Id = 3, Name = "Excavator", TimeStamp = DateTime.Now });

        }
    }
}
