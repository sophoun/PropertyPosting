﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains
{
    public class Location
    {
        [Key]
        public long Id { set; get; }
        public decimal Longitude { set; get; }
        public decimal Latitude { set; get; }
        public string LocationName { get; set; }
    }
}