﻿using PropertyPosting.Controllers.ViewModels.ApiViewModels.Home;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyPosting.Hubs
{
   public interface IPostHomeHub
    {
        void PostHome(PostApiResponse<HomeApiRequestResponse> t);
        void UpdatePostHome(PostApiResponse<HomeApiRequestResponse> t);
        void DeletePostHome(long t);
    }
}
