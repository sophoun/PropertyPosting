﻿using PropertyPosting.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using PropertyPosting.Models.Domains;

namespace PropertyPosting.Configures.ConfigDB
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(): base("PropertyPosting") {

        }

        public DbSet<EntertainmentPost> EntertainmentPosts { set; get; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Home> Homes { get; set; }
        public DbSet<Land> Lands { get; set; }
        public DbSet<PostImage> PostImages { get; set; }
        public DbSet<PostType> PostTypes { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<User> Users { get; set; }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = this.ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added ||
                            e.State == EntityState.Modified ||
                            e.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
                entry.State = EntityState.Detached;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            

        }
    }
}