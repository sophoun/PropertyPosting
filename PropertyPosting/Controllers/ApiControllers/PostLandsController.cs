﻿using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Land;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Location;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response;
using PropertyPosting.Hubs;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;
using PropertyPosting.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace PropertyPosting.Controllers.ApiControllers
{
    [Route("api/post/land")]
    public class PostLandsController : BaseApiController<PostApiRequest<LandApiRequestResponse>>
    {
        private readonly IMapper _mapper;

        private IPostRepository _postService;
        private IRepository<Location> _locationService;
        private IPostImageRepository _postImageService;

        private readonly IRepository<Land> _landService;
        private IRepository<PostType> _postTypeService;
        private readonly IRepository<User> _userService;
        private readonly IPostLandHub _postLandHub;
        public PostLandsController(IMapper mapper, IPostRepository postService, 
            IRepository<Location> locationService, IPostImageRepository postImageService,
            IRepository<Land> landService, IRepository<PostType> postTypeService,
            IRepository<User> userService, IPostLandHub postLandHub) : base(mapper)
        {
            _mapper = mapper;
            _postService = postService;
            _locationService = locationService;
            _postImageService = postImageService;
            _landService = landService;
            _postTypeService = postTypeService;
            _userService = userService;
            _postLandHub = postLandHub;
        }
        [Route("api/post/land/{id}")]
        public override IHttpActionResult Delete(long id)
        {
            var post = _postService.GetById(id);
            if (post == null)
                return NotFound();
            _postService.Delete(post);

            var location = _locationService.GetById(id);
            if (location != null)
                _locationService.Delete(location);

            var land = _landService.GetById(id);
            if (land != null)
                _landService.Delete(land);

            var images = _postImageService.GetAll().Where(i => i.PostId == id);
            foreach (var postImage in images)
                _postImageService.Delete(postImage);
            _postLandHub.DeletePostLand(id);
            return Ok();
        }

        public override IHttpActionResult Get()
        {
            var posts = _postService.GetAllLandPosts();
            var postLandResponse = new List<PostApiResponse<LandApiRequestResponse>>();
            foreach (var post in posts)
            {
                var postLand = _mapper.Map<PostApiResponse<LandApiRequestResponse>>(post);
                postLand.Location = _mapper.Map<LocationApiRequestResponse>(_locationService.GetById(post.LocationId));
                postLand.PostDetail = _mapper.Map<LandApiRequestResponse>(_landService.GetById(post.LandId));
                postLand.PostType = _mapper.Map<PostTypeApiResponse>(_postTypeService.GetById(post.PostTypeId));
                postLand.User = _mapper.Map<UserApiResponse>(_userService.GetById(post.UserId));

                var postImages = _postImageService.GetByPostId(post.Id);
                if (postImages != null)
                {
                    postLand.Images = new string[postImages.Count()];
                    for (var i = 0; i < postImages.Count(); i++)
                    {
                        postLand.Images[i] = postImages[i].Image;
                    }
                }
                postLandResponse.Add(postLand);
            }
            return Json(postLandResponse);
        }

        private PostApiResponse<LandApiRequestResponse> GetPostLand(long id)
        {
            var post = _postService.GetById(id);
            var postLand = _mapper.Map<PostApiResponse<LandApiRequestResponse>>(post);
            postLand.Location = _mapper.Map<LocationApiRequestResponse>(_locationService.GetById(post.LocationId));
            postLand.PostDetail = _mapper.Map<LandApiRequestResponse>(_landService.GetById(post.LandId));
            postLand.PostType = _mapper.Map<PostTypeApiResponse>(_postTypeService.GetById(post.PostTypeId));
            postLand.User = _mapper.Map<UserApiResponse>(_userService.GetById(post.UserId));

            var postImages = _postImageService.GetByPostId(post.Id);
            if (postImages != null)
            {
                postLand.Images = new string[postImages.Count()];
                for (var i = 0; i < postImages.Count(); i++)
                {
                    postLand.Images[i] = postImages[i].Image;
                }
            }
            return postLand;
        }

        [Route("api/post/land/{id}")]
        public override IHttpActionResult Get(long id)
        {
            var postLand = GetPostLand(id);
            return Json(postLand);
        }


        public override IHttpActionResult Post(PostApiRequest<LandApiRequestResponse> t)
        {
            var post = _mapper.Map<Post>(t);
            var location = _mapper.Map<Location>(t.Location);
            var land = _mapper.Map<Land>(t.PostDetail);

            _locationService.Insert(location);
            _landService.Insert(land);

            post.LocationId = location.Id;
            post.LandId = land.Id;
            _postService.Insert(post);

            foreach (var image in t.Images)
            {
                var postImage = new PostImage()
                {
                    PostId = post.Id,
                    Image = ImageUtil.SavePostImage(image, ImageUtil.GetImageFileName(post.Title))
                };

                _postImageService.Insert(postImage);
            }
            var postLand =GetPostLand(post.Id);
            _postLandHub.PostLand(postLand);
            return Ok();
        }

        [Route("api/post/land/{id}")]
        public override IHttpActionResult Put(long id, PostApiRequest<LandApiRequestResponse> t)
        {
            var post = _postService.GetById(id);
            if (post == null)
            {
                return BadRequest("Post Not Found");
            }
            var location = _mapper.Map<Location>(t.Location);
            location.Id = post.LocationId;
            _locationService.Update(location);
            var land = _mapper.Map<Land>(t.PostDetail);
            land.Id = post.LandId;
            _landService.Update(land);
            _mapper.Map(t, post);
            post.LocationId = location.Id;
            post.LandId = land.Id;

            _postService.Update(post);
            var postLand = GetPostLand(post.Id);
            _postLandHub.UpdateLand(postLand);
            return Ok();
        }
    }
}