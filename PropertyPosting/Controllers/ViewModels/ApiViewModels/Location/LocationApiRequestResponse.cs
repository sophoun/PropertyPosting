﻿namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Location
{
    public class LocationApiRequestResponse
    {
        public int Id { set; get; }
        public decimal Longitude { set; get; }
        public decimal Latitude { set; get; }
        public string LocationName { get; set; }
    }
}