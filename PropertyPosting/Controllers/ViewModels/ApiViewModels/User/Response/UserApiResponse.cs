﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response { 
    public class UserApiResponse
    {
        [Required]
        public long Id { set; get; }
        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        [Required]
        public string Sex { set; get; }
        [Required]
        public DateTime? Dob { set; get; } // DateTime format:: "MM/DD/YYYY HH:MM"
        [Required]
        public string Address { set; get; }
        [Required]
        public string AccountType { set; get; }
        [Required]
        public string UserName { set; get; }
        public string Phone { set; get; }
        public string Image { set; get; }
        [Required]
        public string Email { set; get; }
        public string Other { set; get; }
    }
}