﻿using System;
using System.ComponentModel.DataAnnotations;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Staff.Request
{
    public class StaffApiViewModel
    {
        [Required]
        public string FirstName { set; get; }
        [Required]
        public string LastName { set; get; }
        [Required]
        public string Sex { set; get; }
        [Required]
        public DateTime? Dob { set; get; }
        public string Address { set; get; }
        [Required]
        public AccountType AccountType { set; get; }
        [Required]
        public string UserName { set; get; }
        [Required]
        public string Password { set; get; }
        public string IdCard { set; get; }
        public string Phone { set; get; }
        public string Image { set; get; }
        public string Email { set; get; }
        public string Other { set; get; }
    }
}