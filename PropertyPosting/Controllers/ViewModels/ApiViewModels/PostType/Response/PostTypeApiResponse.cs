﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response
{
    public class PostTypeApiResponse
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { set; get; }
        public string Other { set; get; }
        public DateTime TimeStamp { set; get; }
    }
}