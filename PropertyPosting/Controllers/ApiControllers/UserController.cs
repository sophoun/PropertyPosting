﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Controllers.ApiControllers
{
    public class UserController : BaseApiController<UserApiRequest>
    {
        private readonly IRepository<User> _userService;
        private readonly IMapper _mapper;

        public UserController(IMapper mapper, IRepository<User> userService) : base(mapper)
        {
            _mapper = mapper;
            _userService = userService;
        }

        public override IHttpActionResult Get()
        {
            var users = _userService.GetAll();

            var usersResponse = _mapper.Map<IEnumerable<User>, IEnumerable<UserApiResponse>>(users);

            return Json(usersResponse);
        }

        public override IHttpActionResult Get(long id)
        {
            var user = _userService.GetById(id);
            if (user is null)
                return NotFound();
            else
                return Json(_mapper.Map<UserApiResponse>(user));
        }

        public override IHttpActionResult Post(UserApiRequest t)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                var user = _mapper.Map<User>(t);
                _userService.Insert(user);
                return Content(HttpStatusCode.Created, _mapper.Map<UserApiResponse>(user));
            }
        }

        public override IHttpActionResult Put(long id, UserApiRequest t)
        {
            var user = _mapper.Map<User>(t);
            user.Id = id;
            _userService.Update(user);
            return Json(_mapper.Map<UserApiResponse>(user));
        }

        public override IHttpActionResult Delete(long id)
        {
            var user = _userService.GetById(id);
            _userService.Delete(user);
            return Ok();
        }
    }
}
