﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains.Enums
{
    public enum PostStatus
    {
        Posting,
        Sold,
        Stop
    }
}