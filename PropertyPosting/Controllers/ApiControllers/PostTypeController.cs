﻿using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Controllers.ApiControllers
{
    public class PostTypeController : BaseApiController<PostTypeApiRequest>
    {
        private readonly IRepository<PostType> _postTypeService;
        private readonly IMapper _mapper;

        public PostTypeController(IMapper mapper, IRepository<PostType> postTypeService) : base(mapper)
        {
            _mapper = mapper;
            _postTypeService = postTypeService;
        }

        public override IHttpActionResult Get()
        {
            var postTypes = _postTypeService.GetAll();
            var result = _mapper.Map<IEnumerable<PostTypeApiResponse>>(postTypes);
            return Json(result);
        }

        public override IHttpActionResult Get(long id)
        {
            var postType = _postTypeService.GetById(id);
            if (postType == null)
                return NotFound();
            else
                return Json(_mapper.Map<PostTypeApiResponse>(postType));
        }

        public override IHttpActionResult Post(PostTypeApiRequest t)
        {
            var postType = _mapper.Map<PostType>(t);
            _postTypeService.Insert(postType);
            return Json(postType);
        }

        public override IHttpActionResult Put(long id, PostTypeApiRequest t)
        {
            var postType = _mapper.Map<PostType>(t);
            postType.Id = (int) id;
            _postTypeService.Update(postType);
            return Json(postType);
        }

        public override IHttpActionResult Delete(long id)
        {
            var postType = _postTypeService.GetById(id);
            if (postType == null)
                return NotFound();
            _postTypeService.Delete(postType);
            return Ok();
        }
    }
}