﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PropertyPosting.Models.Domains
{
    public class EntertainmentPost
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public string Title { set; get; }
        public string Description { set; get; }
        [Required]
        public string Video { set; get; }
        [Required]
        public DateTime TimeStamp { set; get; }
        public string Other { set; get; }
    }
}