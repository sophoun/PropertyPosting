﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Models.Domains
{
    public class Home
    {
        [Key]
        public long Id { set; get; }
        [Required]
        public decimal Width { set; get; }
        [Required]
        public decimal Length { set; get; }
        [Required]
        public int Bedroom { set; get; }
        [Required]
        public int Bathroot { set; get; }
        [Required]
        public int Livingroot { set; get; }
        [Required]
        public int ParkingLot { set; get; }
        [Required]
        public HomeType HomeType { set; get; } // villa or flat
        [Required]
        public decimal Floor { set; get; }
        public string Other { set; get; }
    }
}
