﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PropertyPosting.Tools
{
    public class Const
    {
        // ---> Domain enum string value
        // AccountType
        public static readonly string Administrator = "Administrator";
        public static readonly string User = "User";

        // AllowedType
        public static readonly string Pending = "Pending";
        public static readonly string Allowed = "Allowed";
        public static readonly string Deny = "Deny";

        // HomeType
        public static readonly string Villa = "Villa";
        public static readonly string Flat = "Flat";

        // PostType
        public static readonly string Posting = "Posting";
        public static readonly string Sold = "Sold";
        public static readonly string Stop = "Stop";

        // PropertyCondition
        public static readonly string Rent = "Rent";
        public static readonly string Sale = "Sale";
        // <---
    }
}