namespace PropertyPosting.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddpostIdtoImageTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostImages", "PostId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostImages", "PostId");
        }
    }
}
