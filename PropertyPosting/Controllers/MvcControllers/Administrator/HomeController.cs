﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using PropertyPosting.Controllers.Base;

namespace PropertyPosting.Controllers.MvcControllers.Administrator
{
    public class HomeController : BaseController
    {
        public HomeController(IMapper mapper) : base(mapper)
        {
        }

        public override ActionResult Index()
        {
            return View("~/Views/Administrator/Home/Index.cshtml");
        }

        public ActionResult PostHome()
        {
            return View();
        }
        public override ActionResult Details(long id)
        {
            throw new NotImplementedException();
        }
    }
}