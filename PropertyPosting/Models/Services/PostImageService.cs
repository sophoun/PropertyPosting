﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class PostImageService : IPostImageRepository
    {
        private readonly AppDbContext _appDbContext;

        public PostImageService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(PostImage t)
        {
            _appDbContext.PostImages.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(PostImage t)
        {
            _appDbContext.PostImages.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(PostImage t)
        {
            _appDbContext.PostImages.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<PostImage> GetAll()
        {
            return _appDbContext.PostImages.ToList();
        }

        public PostImage GetById(long id)
        {
            return _appDbContext.PostImages.Find(id);
        }

        public List<PostImage> GetByPostId(long id)
        {
            return _appDbContext.PostImages
                .Where(i => i.PostId == id)
                .ToList();
        }
    }
}