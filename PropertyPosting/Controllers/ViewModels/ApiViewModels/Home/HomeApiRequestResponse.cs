﻿using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Controllers.ViewModels.ApiViewModels.Home
{
    public class HomeApiRequestResponse
    {
        public decimal Width { set; get; }
        public decimal Length { set; get; }
        public int Bedroom { set; get; }
        public int Bathroot { set; get; }
        public int Livingroot { set; get; }
        public int ParkingLot { set; get; }
        public string HomeType { set; get; } // villa or flat
        public decimal Floor { set; get; }
        public string Other { set; get; }
    }
}
