﻿using System.Net;
using System.Web.Http;
using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Staff.Request;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Controllers.ApiControllers
{
    public class StaffController : BaseApiController<StaffApiViewModel>
    {
        private readonly IRepository<Staff> _staffService;
        private readonly IMapper _mapper;

        public StaffController(IMapper mapper, IRepository<Staff> staffService) : base(mapper)
        {
            _mapper = mapper;
            _staffService = staffService;
        }

        public override IHttpActionResult Get()
        {
            var staffs = _staffService.GetAll();
            return Json(staffs);
        }

        public override IHttpActionResult Get(long id)
        {
            var staff = _staffService.GetById(id);
            if (staff is null)
                return NotFound();
            else
                return Json(staff);
        }

        public override IHttpActionResult Post(StaffApiViewModel t)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                var staff = _mapper.Map<Staff>(t);
                _staffService.Insert(staff);
                return Content(HttpStatusCode.Created, staff);
            }
        }

        public override IHttpActionResult Put(long id, StaffApiViewModel t)
        {
            var staff = _mapper.Map<Staff>(t);
            staff.Id = id; 
            _staffService.Update(staff);
            return Json(staff);
        }

        public override IHttpActionResult Delete(long id)
        {
            var staff = _staffService.GetById(id);
            _staffService.Delete(staff);
            return Ok();
        }
    }
}
