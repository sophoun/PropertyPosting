﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using PropertyPosting.Models.Domains.Enums;

namespace PropertyPosting.Models.Domains
{
    public class Staff
    {
        [Key]
        public long Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Sex { set; get; }
        public DateTime? Dob { set; get; }
        public string Address { set; get; }
        public AccountType AccountType { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }
        public string IdCard { set; get; }
        public string Phone { set; get; }
        public string Image { set; get; }
        public string Email { set; get; }
        public DateTime TimeStamp { set; get; }
        public string Other { set; get; }
    }
}