﻿using Microsoft.AspNet.SignalR;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Land;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;

namespace PropertyPosting.Hubs
{
    public  interface IPostLandHub
    {
        void DeletePostLand(long id);
        void PostLand(PostApiResponse<LandApiRequestResponse> t);
        void UpdateLand(PostApiResponse<LandApiRequestResponse> t);
    }
}
