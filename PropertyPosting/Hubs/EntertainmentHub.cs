﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Entertainment;

namespace PropertyPosting.Hubs
{
    public class EntertainmentHub : Hub, IEntertainmentHub
    {
        private readonly IHubContext _hubContext;
        public EntertainmentHub()
        {
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<EntertainmentHub>();
        }
        [HubMethodName("deleteEntertainment")]
        public void DeleteEntertainment(long t)
        {
            _hubContext.Clients.All.DeleteEntertainment(t);
        }

        [HubMethodName("postEntertainment")]
        public void PostEntertainment(EntertainmentApiRequestResponse t)
        {
            _hubContext.Clients.All.PostEntertainment(t);            
        }
        [HubMethodName("updateEntertainment")]
        public void UpdateEntertainment(EntertainmentApiRequestResponse t)
        {
            _hubContext.Clients.All.UpdateEntertainment(t);
        }
    }
}