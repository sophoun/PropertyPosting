﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PropertyPosting.Configures.ConfigDB;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;

namespace PropertyPosting.Models.Services
{
    public class EntertainmentPostService : IRepository<EntertainmentPost>
    {
        private readonly AppDbContext _appDbContext;

        public EntertainmentPostService(AppDbContext appDbContext)
        {
            this._appDbContext = appDbContext;
        }

        public void Insert(EntertainmentPost t)
        {
            _appDbContext.EntertainmentPosts.Add(t);
            _appDbContext.SaveChanges();
        }

        public void Update(EntertainmentPost t)
        {
            _appDbContext.EntertainmentPosts.Attach(t);
            _appDbContext.Entry(t).State = EntityState.Modified;
            _appDbContext.SaveChanges();
        }

        public void Delete(EntertainmentPost t)
        {
            _appDbContext.EntertainmentPosts.Remove(t);
            _appDbContext.SaveChanges();
        }

        public IEnumerable<EntertainmentPost> GetAll()
        {
            return _appDbContext.EntertainmentPosts.ToList();
        }

        public EntertainmentPost GetById(long id)
        {
            return _appDbContext.EntertainmentPosts.Find(id);
        }
    }
}