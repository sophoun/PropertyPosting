﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using AutoMapper;

namespace PropertyPosting.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        protected readonly IMapper Mapper;

        protected BaseController(IMapper mapper)
        {
            this.Mapper = mapper;
        }

        // GET: /{controller}
        public abstract ActionResult Index();

        // GET: /{controller}/Details/{id}
        public abstract ActionResult Details(long id);

    }
}