namespace PropertyPosting.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updateprimarykey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Homes");
            DropPrimaryKey("dbo.Lands");
            DropPrimaryKey("dbo.Locations");
            AddColumn("dbo.Homes", "Id", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.Lands", "Id", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.Locations", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.Homes", "Id");
            AddPrimaryKey("dbo.Lands", "Id");
            AddPrimaryKey("dbo.Locations", "Id");
            DropColumn("dbo.Homes", "PostId");
            DropColumn("dbo.Lands", "PostId");
            DropColumn("dbo.Locations", "PostId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Locations", "PostId", c => c.Long(nullable: false));
            AddColumn("dbo.Lands", "PostId", c => c.Long(nullable: false));
            AddColumn("dbo.Homes", "PostId", c => c.Long(nullable: false));
            DropPrimaryKey("dbo.Locations");
            DropPrimaryKey("dbo.Lands");
            DropPrimaryKey("dbo.Homes");
            DropColumn("dbo.Locations", "Id");
            DropColumn("dbo.Lands", "Id");
            DropColumn("dbo.Homes", "Id");
            AddPrimaryKey("dbo.Locations", "PostId");
            AddPrimaryKey("dbo.Lands", "PostId");
            AddPrimaryKey("dbo.Homes", "PostId");
        }
    }
}
