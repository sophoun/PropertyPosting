namespace PropertyPosting.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initializedatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntertainmentPosts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Video = c.String(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Other = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Homes",
                c => new
                    {
                        PostId = c.Long(nullable: false),
                        Width = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Length = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Bedroom = c.Int(nullable: false),
                        Bathroot = c.Int(nullable: false),
                        Livingroot = c.Int(nullable: false),
                        ParkingLot = c.Int(nullable: false),
                        HomeType = c.Int(nullable: false),
                        Floor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Other = c.String(),
                    })
                .PrimaryKey(t => t.PostId);
            
            CreateTable(
                "dbo.Lands",
                c => new
                    {
                        PostId = c.Long(nullable: false),
                        LandWidth = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LandLength = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Other = c.String(),
                    })
                .PrimaryKey(t => t.PostId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        PostId = c.Long(nullable: false),
                        Longitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Latitude = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LocationName = c.String(),
                    })
                .PrimaryKey(t => t.PostId);
            
            CreateTable(
                "dbo.PostImages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LocationId = c.Long(nullable: false),
                        Status = c.Int(nullable: false),
                        PropertyCondition = c.Int(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Allowed = c.Int(nullable: false),
                        AllowedDate = c.DateTime(nullable: false),
                        Repost = c.Long(nullable: false),
                        Other = c.String(),
                        PostTypeId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        StaffId = c.Long(nullable: false),
                        HomeId = c.Long(nullable: false),
                        LandId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Other = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Sex = c.String(),
                        Dob = c.DateTime(),
                        Address = c.String(),
                        AccountType = c.Int(nullable: false),
                        UserName = c.String(),
                        Password = c.String(),
                        IdCard = c.String(),
                        Phone = c.String(),
                        Image = c.String(),
                        Email = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        Other = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Sex = c.String(),
                        Dob = c.DateTime(),
                        Address = c.String(),
                        AccountType = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                        Phone = c.String(),
                        Image = c.String(),
                        Email = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        Other = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.Staffs");
            DropTable("dbo.PostTypes");
            DropTable("dbo.Posts");
            DropTable("dbo.PostImages");
            DropTable("dbo.Locations");
            DropTable("dbo.Lands");
            DropTable("dbo.Homes");
            DropTable("dbo.EntertainmentPosts");
        }
    }
}
