﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using PropertyPosting.Controllers.Base;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Home;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Location;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Request;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.PostType.Response;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.User.Response;
using PropertyPosting.Hubs;
using PropertyPosting.Models.Domains;
using PropertyPosting.Models.Repository;
using PropertyPosting.Tools;

namespace PropertyPosting.Controllers.ApiControllers
{
    [Route("api/post/home")]
    public class PostHomeController : BaseApiController<PostApiRequest<HomeApiRequestResponse>>
    {
        private readonly IPostRepository _postService;
        private readonly IRepository<Location> _locationService;
        private readonly IPostImageRepository _postImageService;
        private readonly IRepository<Home> _homeService;
        private readonly IRepository<PostType> _postTypeService;
        private readonly IRepository<User> _userService;
        private readonly IMapper _mapper;
        private readonly IPostHomeHub _postHomeHub;
        public PostHomeController(IMapper mapper,
            IPostRepository postService, 
            IRepository<Location> locationService, 
            IPostImageRepository postImageService, 
            IRepository<Home> homeService,
            IRepository<PostType> postTypeService,
            IRepository<User> userService,
            IPostHomeHub postHomeHub) : base(mapper)
        {
            _mapper = mapper;
            _postService = postService;
            _locationService = locationService;
            _postImageService = postImageService;
            _homeService = homeService;
            _postTypeService = postTypeService;
            _userService = userService;
            _postHomeHub = postHomeHub;
        }

        public override IHttpActionResult Get()
        {
            var posts = _postService.GetAllHomePosts();
            var postHomeResponse = new List<PostApiResponse<HomeApiRequestResponse>>();
            foreach (var post in posts)
            {
                var postHome = _mapper.Map<PostApiResponse<HomeApiRequestResponse>>(post);
                postHome.Location = _mapper.Map<LocationApiRequestResponse>(_locationService.GetById(post.LocationId));
                postHome.PostDetail = _mapper.Map<HomeApiRequestResponse>(_homeService.GetById(post.HomeId));
                postHome.PostType = _mapper.Map<PostTypeApiResponse>(_postTypeService.GetById(post.PostTypeId));
                postHome.User = _mapper.Map<UserApiResponse>(_userService.GetById(post.UserId));

                var postImages = _postImageService.GetByPostId(post.Id);
                if (postImages != null)
                {
                    postHome.Images = new string[postImages.Count()];
                    for (var i = 0; i < postImages.Count(); i++)
                    {
                        postHome.Images[i] = postImages[i].Image;
                    }
                }
                postHomeResponse.Add(postHome);
            }
            return Json(postHomeResponse);
        }

        private PostApiResponse<HomeApiRequestResponse> GetPostHome(long id)
        {
            var post = _postService.GetById(id);
            var postHome = _mapper.Map<PostApiResponse<HomeApiRequestResponse>>(post);
            postHome.Location = _mapper.Map<LocationApiRequestResponse>(_locationService.GetById(post.LocationId));
            postHome.PostDetail = _mapper.Map<HomeApiRequestResponse>(_homeService.GetById(post.HomeId));
            postHome.PostType = _mapper.Map<PostTypeApiResponse>(_postTypeService.GetById(post.PostTypeId));
            postHome.User = _mapper.Map<UserApiResponse>(_userService.GetById(post.UserId));

            var postImages = _postImageService.GetByPostId(post.Id);
            if (postImages != null)
            {
                postHome.Images = new string[postImages.Count()];
                for (var i = 0; i < postImages.Count(); i++)
                {
                    postHome.Images[i] = postImages[i].Image;
                }
            }
            return postHome;
        }

        [Route("api/post/home/{id}")]
        public override IHttpActionResult Get(long id)
        {
            var postHome = GetPostHome(id);
            return Json(postHome);
        }

        public override IHttpActionResult Post(PostApiRequest<HomeApiRequestResponse> t)
        {
            var post = _mapper.Map<Post>(t);
            var location = _mapper.Map<Location>(t.Location);
            var home = _mapper.Map<Home>(t.PostDetail);

            _locationService.Insert(location);
            _homeService.Insert(home);

            post.LocationId = location.Id;
            post.HomeId = home.Id;
            post.PostTypeId = 1;
            _postService.Insert(post);

            foreach (var image in t.Images)
            {
                var postImage = new PostImage()
                {
                    PostId = post.Id,
                    Image = ImageUtil.SavePostImage(image, ImageUtil.GetImageFileName(post.Title))
                };
                
                _postImageService.Insert(postImage);
            }

            var postHome = GetPostHome(post.Id);
            _postHomeHub.PostHome(postHome);
            return Ok();
        }

        [Route("api/post/home/{id}")]
        public override IHttpActionResult Put(long id, PostApiRequest<HomeApiRequestResponse> t)
        {
            var post = _postService.GetById(id);
            if (post == null)
                return NotFound();

            _mapper.Map(t, post);

            var location = _mapper.Map<Location>(t.Location);
            var home = _mapper.Map<Home>(t.PostDetail);

            location.Id = post.LocationId;
            _locationService.Update(location);

            home.Id = post.HomeId;
            _homeService.Update(home);
            post.PostTypeId = 2;
            _postService.Update(post);
            var postHome = GetPostHome(post.Id);
            _postHomeHub.UpdatePostHome(postHome);
            return Ok();
        }

        [Route("api/post/home/{id}")]
        public override IHttpActionResult Delete(long id)
        {
            var post = _postService.GetById(id);
            if (post == null)
                return NotFound();
            _postService.Delete(post);

            var location = _locationService.GetById(id);
            if (location != null)
                _locationService.Delete(location);

            var home = _homeService.GetById(id);
            if (home != null)
                _homeService.Delete(home);

            var images = _postImageService.GetAll().Where(i => i.PostId == id);
            foreach (var postImage in images)
                _postImageService.Delete(postImage);
            _postHomeHub.DeletePostHome(post.Id);
            return Ok();
        }
    }
}
