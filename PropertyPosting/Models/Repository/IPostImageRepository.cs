﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyPosting.Models.Domains;

namespace PropertyPosting.Models.Repository
{
    public interface IPostImageRepository : IRepository<PostImage>
    {
        List<PostImage> GetByPostId(long id);
    }
}
