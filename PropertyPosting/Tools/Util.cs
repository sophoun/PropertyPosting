﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PropertyPosting.Tools
{
    public static class Util
    {
        // Encrypt string to byte array
        public static string Encrypt(string stringToEncrypt)
        {
            var security = Security.Instance;
            return security.Encrypt(stringToEncrypt);
        }

        // Decrypt byte array to string
        public static string Decrypt(string stringToDecrypt)
        {
            var security = Security.Instance;
            return security.Decrypt(stringToDecrypt);
        }

    }
}