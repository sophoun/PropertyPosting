﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Home;
using PropertyPosting.Controllers.ViewModels.ApiViewModels.Post.Response;

namespace PropertyPosting.Hubs
{
    public class PostHomeHub:  Hub,IPostHomeHub
    {
        private readonly IHubContext _hubContext;
        public PostHomeHub()
        {
            _hubContext = GlobalHost.ConnectionManager.GetHubContext<PostHomeHub>();
        }
        

        [HubMethodName("deletePostHome")]
        public void DeletePostHome(long t)
        {
            _hubContext.Clients.All.DeletePostHome(t);
        }
        [HubMethodName("postHome")]
        public void PostHome(PostApiResponse<HomeApiRequestResponse> t)
        {
            _hubContext.Clients.All.PostHome(t);
        }
        [HubMethodName("updatePostHome")]
        public void UpdatePostHome(PostApiResponse<HomeApiRequestResponse> t)
        {
            _hubContext.Clients.All.UpdatePostHome(t);
        }
    }
}