﻿//Author James Harrington 2014
function base64(files, callback) {
    function readFile(files, index) {
        if ( index >= files.length) {
            return;
        }
        var coolFile = {};
        function readerOnload(e) {
            var base64 = btoa(e.target.result);
            coolFile.base64 = base64;
            callback(coolFile);
            readFile(files, index + 1);
        };

        var reader = new FileReader();
        reader.onload = readerOnload;
        var file = files[index];
        coolFile.filetype = file.type;
        coolFile.size = file.size;
        coolFile.filename = file.name;
        reader.readAsBinaryString(file);
    }
    readFile(files, 0);
}