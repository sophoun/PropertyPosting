﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace PropertyPosting
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Register Unity Dependency injection for Web.Api
            UnityConfig.RegisterComponents();
            UnityConfig.RegisterDependencyResolverForWebApi(config);


            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.EnsureInitialized();

            // Add support return type json format for Web API
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
