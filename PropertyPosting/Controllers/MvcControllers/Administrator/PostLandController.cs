﻿using AutoMapper;
using PropertyPosting.Controllers.Base;
using System.Web.Mvc;

namespace PropertyPosting.Controllers.MvcControllers.Administrator
{
    public class PostLandController : BaseController
    {
        private readonly IMapper _mapper;
        public PostLandController(IMapper mapper): base(mapper)
        {
            _mapper = mapper;
        }
        public override ActionResult Details(long id)
        {

            return View();
        }

        public ActionResult Create()
        {

            return View();
        }

        public override ActionResult Index()
        {
            return View();
        }
    }
}